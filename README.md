Dans ton répertoire, par ordre chronologique :
1) git pull (tu récupères tout ce qu'il y a sur ton compte GitLab)
2) Créer/modifier des fichiers 
3) Ajouter à ton répertoire : git add . (Pour tout ajouter) ou git add fichier1 fichier2 ...
4) Commiter : git commit -m "Commentaire à mon commit"
5) L'envoyer sur la plateforme : git push 
6) Vérifier que ton répertoire est à jour (s'il manque un "add", un "commit", etc) : git status
